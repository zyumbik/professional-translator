# ✏️ Readme
Professional translator is a small project which is being done completely open source — from the **idea stage**. Our personal objective in this project is to familiarize ourselves with the open source world. We also want to develop and bring to life our vision of the way to build open source projects.

# Idea

It’s like Google Translate but for the phrases which are commonly said by professionals and their clients. For example, a designer could enter a phrase which was said by their client and get to know what they “actually” wanted to say. 
This project is meant to entertain people, not solve their actual communication problem (although in some cases it might be helpful). Therefore, most “translations“ would probably be a bit witty or humorous.

# Goal

The goal of this project is to raise awareness of the “professional — client” communication problem and have some fun along the way! 🤗

# MVP

It is reasonable to release the first version as soon as possible, so it will only have the basic functionality. It would be too much to write translations for all professionals so the first version would be limited to the translations between designers and their clients. Just because we know them the best. (If you would like to add other’s phrases — you are more than welcome!)

# Process

We are generally following [Design Thinking](https://en.wikipedia.org/wiki/Design_thinking) framework. However to be productive we also take some exercises and activities from the [Google Design Sprint](https://designsprintkit.withgoogle.com/). After the design stage is finished and the prototypes are ready, we will start coding it. It’s uncertain what frameworks are going to be used as we are not sure about it’s functionality and look yet.

# Contribute
- Read the [contribution guidelines](https://paper.dropbox.com/doc/Contributing-guidelines--AKZegrIJkhN8MHIpFDFgHvA4AQ-ZvSZLuat8mr2H9CuGvCkQ). 🌈
- [Suggest phrases and translations](https://docs.google.com/spreadsheets/d/1d44DNxPYtfDeA7LpZVVIYGnqoXAWFGlUqtwuNJIskYA/edit?usp=sharing) for designers and clients (maybe other professions too). A good source of phrases is [social media](https://twitter.com/thefuturishere/status/1029846953054617600), you just need to come up with a nice answer to them.
- [Learn about the process](https://paper.dropbox.com/doc/Process--AKWm34fdfOmVfZohOr6ENOxhAQ-6HIlAXTSNjbfTIlpapeBp) and what stage the project is currently on. You may jump in on any stage and add your work to the project. It would be helpful if you tell us about your plans before you start though.
- [Read our guide](https://paper.dropbox.com/doc/Working-with-open-source-docs--AKXlFixTPWRYVOZT5vUyQdmTAQ-drekJsRRZPYLgcXhppTNv) on the creation and usage of the open source docs and then use them to share your work with the community.

Feel free to say “hi”, send feedback or your ideas — [@Wersatiles](https://twitter.com/Wersatiles). We’d love to talk! 😊

