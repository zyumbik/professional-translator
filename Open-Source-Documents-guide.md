# 📄 Open Source Documents guide
A small guide on how to work with open source documents like this one.

**work** /wəːk/ *verb* — **be engaged in physical or mental activity in order to achieve a result; do work.
**open-source** /əʊp(ə)nˈsɔːs/ *adjective* — denoting software for which the original source code is made freely available and may be redistributed and modified.
**document** /ˈdɒkjʊm(ə)nt/ *noun* — a piece of written, printed, or electronic matter that provides information or evidence or that serves as an official record.

# 🤷‍♂️ What are open source documents?

In the context of our initiative to make design process accessible to everyone, open source documents are those that are made publicly available for editing by anyone. Currently we will be creating them in Paper in the absence of a better alternative. **They are open for editing for everyone with a link.**

(If you know a better way to do it — [*please tell us*](https://twitter.com/Wersatiles/status/1029988491835437056)!)

# 🕵️‍♀️ What are they used for?

Open source documents could be used for anything that is related to the project. Many stages of design could be put in the form of rich text documents. They are created to accelerate the project development or improve existing products. Such documents make everyone able to contribute to the project or simply learn from it. Open source docs may include such information as:

- **Research data** (interviews, online search, other findings)
- **Analysis** (insights, comparisons, conclusions)
- **Ideation** (results of, or real-time collective idea generation)
- **Test results** (what worked, what didn’t, what is missing)
- **Feedback** (comments and chats inside docs)
- *and much more…*
# 👩‍💻 How to work with them?
## Good practices
- **Act with due regard to the other’s work**
- Write in clear and simple (plain) language that everyone understands
- Provide **value**
- Don’t spam or abuse other’s docs
## Participate
- Don’t hesitate to **add** your ideas, insights, parts of the research or any other information if appropriate. However, if you have a lot to add — it may be better create a new document
- Use **comments** to discuss ideas, give feedback or ask questions
## Create

If you made some work on the project and have some information in the form of a text document — share it with the community. You are free to use the services/formats you are comfortable with, however we recommend sticking to Paper for consistency. If you are working on a mockup or any other kind of visual document — we recommend using Figma for that.

To make your document Open Source do the following:

- Make sure the document is **open** for viewing and editing* for anyone with a link
- “Connect” it to the project:
  - Download the document in the preferred format** and **add** it to the project repository
  - Add the link to the Open document in the other file that already belongs to the project
  - Create a link back to the project in the Open doc (could be in a comment)
- It’s optional but recommended to make a private duplicate of your document as a backup.
## Not Open

Documents that

1. don’t contain any links to the project
2. are not linked from the project 
3. are not open for anyone to view and edit*

are **not** considered to be Open and are not a part of the project.

*if the service they are stored on has this option. It’s known that Paper, Google Docs and Figma have this option, so it’s recommended to stick to them and other services that have this option.
** Every project may have different preferences on the formats, so check out project’s contribution guides for details

All that is said above could be applied to this document too.

🙋‍♂️ @Gleb S   🙋‍♀️ @Olesya C 


----------

*(Dropbox, Google, Figma and all other company and product names are trademarks or registered trademarks of their respective companies. They are not affiliated with the creators of this document.)*

