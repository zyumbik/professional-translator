# 👅 Process

[ ] Explain the process better so everybody understands how it would be done and make this list accessible to everyone
## Repository
[ ] Write CONTRIBUTION.md so people know how they could help
[ ] Add licence
  [ ] Lean about different types of licences
  [ ] Decide which one fits the project better — [decision discussion document](https://paper.dropbox.com/doc/License-Decision--AKY1eS~xg3sIfY54I5uwG_wFAQ-Fg09QZd0SraKAF7WQAjtG)
[ ] Improve project structure
[ ] Create a ~~GitLab project icon~~ logo
## Design
[x] A problem
  - Communication between a professional and their client — they don’t always understand each other.
[x] Idea generation
  - Selected idea — translator of the “client” and “professional” languages.
  - [Initial idea presentation](https://paper.dropbox.com/doc/Professional-Language-Translator--AKYku7xD0Q53H8FONg8g2JjkAQ-QgWWqvg3pqt6XJyiUf0lK).
[ ] Storyboards (discover → learn → use → goal)
[ ] Rapid prototyping (develop a solution)
  [ ] More “Crazy 8’s”
  [ ] Try several concepts
  [ ] [deliverable] Testable prototypes (paper)
[ ] Test the prototypes
[ ] Reflect test results in prototypes
[ ] [goal] MVP prototype
## Coding
[ ] Choose the development stack
[ ] Create a more to-do’s
[ ] [goal] Ship the MVP
## Phrases and translations
[ ] Phrases of a client
  [ ] Find at least 10 phrases
  [ ] Come up with translations for them
  [ ] Find more (50?) phrases
  [ ] Translate all
[ ] Phrases of a designer
  [ ] Find at least 10 phrases
  [ ] Come up with translations for them
  [ ] Find more (50?) phrases
  [ ] Translate all
[ ] [goal] A lot of phrases so it’s not boring to use the app
## Organization
[ ] Brand core
[ ] Make money
  [ ] Monetization strategy (find a better way this fun thing could make money)
    Ways we thought of: 
      $  ads
      $  more translations paid
      $  more “languages” paid
      $  V1 — free, V2 — paid
      $  simple 1-time payment
      $  pay what you want
  [ ] Support the contributors if monetization is successful
[ ] Write a funny value proposition
[ ] 
# Ideas for the future
- AI that will understand similar queries and suggest suggest appropriate unique responses.

