# ✏️ Contributing guidelines
We are glad you are reading this because we need volunteer designers, marketers, writers, developers and others to help this project come to fruition. 🍎

# Contributor checklist
- Read contributing guidelines
- Read the license
- Make sure my contribution is consistent with the guidelines
- Code changes are consistent with the coding style
# How to contribute

We’d love to accept your work! Before we can take it, we have to make sure it’s aligned with the guidelines and jump a couple of legal hurdles.

## Licensing of contributed material

Help us decide on the license! 
Read the License carefully before sending us your work. By contributing to this project you agree with the License and understand that anything contributed to the project falls under the licensing terms described in the “License” file. Contribution could be in any form of electronic, verbal or written communication including “Open Source Documents”, source code, email, text, multimedia files and other. Only original materials from you are accepted to the project.

# What to contribute

Glad you asked! Read the [Process](https://paper.dropbox.com/doc/Process--AKZMfDD0dmLAM6HvH03Zo7T9AQ-6HIlAXTSNjbfTIlpapeBp) document and find what hasn’t been done yet, than just do it! Or you can add something valuable to any of the previous actions, for example generate more ideas or interview more people. The most valuable things that help the project grow are thoughts and decisions. So we are expecting to see a lot of **discussions** and deep **research** which both foster highly intentional decisions. Don’t forget that everything should be documented though.

# “Open docs”

We use the term Open docs or Open Source Documents to refer to the documents that are created and/or hosted on the third-party services like Dropbox Paper, Google Docs, Notion or Figma and are considered to be the contribution to this project. To make a contribution in a form of Open Source Document do the following:

- Read our [Open docs guide](https://paper.dropbox.com/doc/Open-Source-Documents-guide--AKbP72b3ksTjnpQ71ctfBO5qAQ-drekJsRRZPYLgcXhppTNv)
- Make sure the document is **open** for viewing and editing for anyone with a link
- “Connect” it to the project:
  - Download the document in the appropriate format and **add** it to the repository (this serves more as a backup than as a working file)
  - Add the link to the Open document in the other file that already belongs to the project
  - Create a link back to the project in the Open doc (could be in a comment)
- *Optional:* create a duplicate of the open file and keep it private (in case something happens with the editable public file)

We chose to make Open docs work this way due to the lack of alternatives. If you know of a better way to do this — [*let us know*](https://twitter.com/Wersatiles/status/1029988491835437056)!

## Open document format

Typically online tools that are used for Open docs have many different export options. You have to pick one of them to add to the repository. How to choose the appropriate format?

- **Text:** since GitLab has markdown preview — export text files in the Markdown (.md) format where possible.
- **Images:** we highly recommend using Figma for all forms of visual communication, especially for mockups and other prototypes. Figma files could be exported in .fig format.
- **Tables:** that’s simple — use CSV.
- **Presentations:** if interactivity is the essential part of the presentation — use .ppt or .key, otherwise export it in the PDF format (preferred).
- When in doubt — use the format that will fit the project the best and use your common sense. 
# Add files to the repository

If you are not familiar with git — it’s definitely worth it to [learn](https://www.google.com/search?q=learn+git) how to [use it](https://medium.com/@dfosco/git-for-designers-856c434716e). However, you can just send your files to us and we will add them to the project if they are accepted.

## Short guide
- [Clone a repository](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html) to your computer.
- Create a [new branch](https://learngitbranching.js.org/). (Always make a new branch for your work, no matter how small. Also, don’t submit unrelated changes in the same branch/pull request.)
- Apply your changes and additions
- [Commit](https://www.git-tower.com/learn/git/commands/git-commit) your changes
- Create a [pull request](https://yangsu.github.io/pull-request-tutorial/)
# Documenting decisions

All design and other decisions should be documented for public reference. While a group of small decisions could be put in a single document, major decisions should be documented in separate files. For example, decisions about some details of a brand identity could be grouped together in one file, and the decision on the product name should be a separate file. 

## Public discussion of a decision

There are several ways of discussing a decision but every decision should be 

1. Create a pull request by filling out the template ([repository](https://gitlab.com/zyumbik/professional-translator/blob/master/Design%20desisions/TEMPLATE.md) or [Paper](https://paper.dropbox.com/doc/Decision-Discussion-Template--AKa48nEFgXPEExy3fJU9AezdAQ-hk75u1rXVpBmsyp4PaW1E)) and saving it under a unique name or creating your own file structure that will work for a given problem.
2. Discuss
3. Document arguments for and against
4. Document the decision
5. Merge the pull request


🙋‍♂️ @Gleb S   🙋‍♀️ @Olesya C 


----------

*(Dropbox, Google, Notion, Figma and all other company and product names are trademarks or registered trademarks of their respective companies. They are not affiliated with the Professional Language Translator and Wersatiles team.)*

